/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.WebDriver;

public class Helper {
    
    public WebDriver switchWindowHelper(WebDriver driver) 
    {
          String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
          String subWindowHandler = null;

          Set<String> handles = driver.getWindowHandles(); // get all window handles
          Iterator<String> iterator = handles.iterator();
          while (iterator.hasNext()){
            subWindowHandler = iterator.next();
          }
          driver.switchTo().window(subWindowHandler); // switch to popup window
          return driver;
    }
    
}
