package me.sleepythread.stepdef.login;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import me.sleepythread.pageobject.bill.BillPage;
import me.sleepythread.pageobject.login.LoginPage;
import me.sleepythread.setdef.Web;

import static org.junit.Assert.assertEquals;

public class Login extends Web{

    private  LoginPage loginPage = new LoginPage(driver);
    private BillPage billPage = new BillPage(driver);

    @Given("^I am on splitwise home page$")
    public void iAmOnSplitwiseHomePage() throws Throwable {
        loginPage.open();
    }

    @When("^I Bill to Friend$")
    public void iBillToFriend() throws Throwable {
        loginPage.fillLoginInfo("sample123@gmail.com","sample123");
        billPage.fillBill();
    }
    
    @Then("^I should see dashboard page$")
    public void iShouldSeeDashboardPage() throws Throwable {
        assertEquals(true,billPage.isBillAdded());
    }

}
