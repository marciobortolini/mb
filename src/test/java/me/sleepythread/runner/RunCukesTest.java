package me.sleepythread.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        glue = {"me.sleepythread.stepdef"},
        features = {"src/test/resources/Features"})
public class RunCukesTest {

}