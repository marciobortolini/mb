package me.sleepythread.setdef;

import org.openqa.selenium.chrome.ChromeDriver;


public abstract class Web {

    static {
        System.setProperty("webdriver.chrome.driver", "/Users/marcio/Desktop/cucumber-java-master/driver/chromedriver");
    }
    final protected ChromeDriver driver = new ChromeDriver();

}
