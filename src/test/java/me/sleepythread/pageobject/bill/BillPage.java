package me.sleepythread.pageobject.bill;

import Helpers.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BillPage {

    private WebDriver driver;

    public BillPage(WebDriver driver){

        this.driver = driver;
    }

    public void fillBill() throws InterruptedException{
          WebDriverWait wait = new WebDriverWait(driver, 90000);
          final WebElement addBill = driver.findElement(By.className("btn-orange"));
          addBill.click();

          final WebElement saveButton = driver.findElement(By.className("submit"));
          
          Helper helper = new Helper();
          driver = helper.switchWindowHelper(driver);
          wait = new WebDriverWait(driver, 90000);
          
          final WebElement userId = driver.findElement(By.id("token-input-add_bill_with"));
          
          userId.sendKeys("flavius@gmail.com");
          final WebElement descriptionBill = driver.findElement(By.className("description"));
          descriptionBill.sendKeys("Sample Bill");
          final WebElement costBill = driver.findElement(By.className("cost"));
          costBill.sendKeys("20");
          
          saveButton.click();
    }

    public boolean isBillAdded(){
        final WebElement dashboardLink = driver.findElement(By.id("dashboard_link"));
        
        return dashboardLink.isDisplayed();
    }
}
