$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 1,
  "name": "Bill Feature",
  "description": "",
  "id": "bill-feature",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "New Bill",
  "description": "",
  "id": "bill-feature;new-bill",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I am on splitwise home page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I Bill to Friend",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see dashboard page",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.iAmOnSplitwiseHomePage()"
});
formatter.result({
  "duration": 4920646808,
  "status": "passed"
});
formatter.match({
  "location": "Login.iBillToFriend()"
});
formatter.result({
  "duration": 3059636695,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"link text\",\"selector\":\"\u003ci class\u003d\"icon-user\"\u003e\u003c/i\u003e flavius\"}\n  (Session info: chrome\u003d62.0.3202.94)\n  (Driver info: chromedriver\u003d2.32.498537 (cb2f855cbc7b82e20387eaf9a43f6b99b6105061),platform\u003dMac OS X 10.12.6 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.5.3\u0027, revision: \u0027a88d25fe6b\u0027, time: \u00272017-08-29T12:42:44.417Z\u0027\nSystem info: host: \u002729656.local\u0027, ip: \u0027fd42:5c63:fd10:0:e8fe:e429:4df4:4fa9\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.12.6\u0027, java.version: \u00271.8.0_151\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{mobileEmulationEnabled\u003dfalse, hasTouchScreen\u003dfalse, platform\u003dMAC, acceptSslCerts\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, platformName\u003dMAC, setWindowRect\u003dtrue, unexpectedAlertBehaviour\u003d, applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.32.498537 (cb2f855cbc7b82e20387eaf9a43f6b99b6105061), userDataDir\u003d/var/folders/kr/580z1m791jsc3jrzvvn5n1k80000gp/T/.org.chromium.Chromium.kWTfGr}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003d, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, version\u003d62.0.3202.94, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, locationContextEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue}]\nSession ID: 9374a2cda0d9e1fc1206e07f6bb03149\n*** Element info: {Using\u003dlink text, value\u003d\u003ci class\u003d\"icon-user\"\u003e\u003c/i\u003e flavius}\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:215)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:167)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:82)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:45)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:646)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:416)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByLinkText(RemoteWebDriver.java:470)\n\tat org.openqa.selenium.By$ByLinkText.findElement(By.java:246)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:408)\n\tat me.sleepythread.pageobject.bill.BillPage.fillBill(BillPage.java:20)\n\tat me.sleepythread.stepdef.login.Login.iBillToFriend(Login.java:25)\n\tat ✽.When I Bill to Friend(login.feature:6)\n",
  "status": "failed"
});
formatter.match({
  "location": "Login.iShouldSeeDashboardPage()"
});
formatter.result({
  "status": "skipped"
});
});